//
//  AppHelper.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

struct Constant {
    
    //API Urls
    static let SERVER_URL = "http://health.mobilfon.ru/stable"
    
    //User Credentials
    static var INNER_TOKEN = KeychainWrapper.standard.string(forKey: "inner_token") ?? ""
    static var USER_PHONE = UserDefaults.standard.string(forKey: "phone") ?? ""
    static let IS_AUTHORIZED = "is_authorized"
    
    //Constant phrases
    static let IN_PROCESSING = "Этот функционал пока еще не реализован"
    
    //Colors
    static let BASE_APP_COLOR = UIColor(0x7431ae)
    static let ADDITIVE_APP_COLOR = UIColor(0xef407d)
    
    //Notifications
    static let CANCEL_ACTION = "CANCEL_ACTION"
    static let CREATE_ACTION = "CREATE_ACTION"
    static let FINISH_ACTION = "FINISH_ACTION"
    
    
}

struct ImageHeaderData {
    static var PNG: [UInt8] = [0x89]
    static var JPEG: [UInt8] = [0xFF]
    static var GIF: [UInt8] = [0x47]
    static var TIFF_01: [UInt8] = [0x49]
    static var TIFF_02: [UInt8] = [0x4D]
}

enum ImageFormat{
    case Unknown, PNG, JPEG, GIF, TIFF
}

extension NSData {
    
    var imageFormat: ImageFormat {
        var buffer = [UInt8](repeating: 0, count: 1)
        
        self.getBytes(&buffer, range: NSRange(location: 0, length: 1))
        
        if buffer == ImageHeaderData.PNG {
            return .PNG
            
        } else if buffer == ImageHeaderData.JPEG {
            return .JPEG
            
        } else if buffer == ImageHeaderData.GIF {
            return .GIF
            
        } else if buffer == ImageHeaderData.TIFF_01 || buffer == ImageHeaderData.TIFF_02 {
            return .TIFF
            
        } else {
            return .Unknown
        }
    }
}

extension UIColor {
    convenience init(_ hex: UInt) {
        self.init(
            red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hex & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension String {
    
    func replaceOccurrences(_ occurrences: [String]) -> String {
        
        var str = self
        
        for occur in occurrences {
            str = str.replacingOccurrences(of: occur, with: "")
        }
        
        print(str)
        
        return str
    }
    
}

extension UIWindow {
    
    func getVisibleVC() -> UIViewController? {
        
        if let vc = UIApplication.shared.keyWindow?.rootViewController {
            return getVC(vc: vc)
        }
        
        return nil
    }
    
    private func getVC(vc: UIViewController) -> UIViewController? {
        
        if let vc = vc as? UINavigationController {
            return vc.topViewController
        } else {
            return vc
        }
        
    }
    
}
