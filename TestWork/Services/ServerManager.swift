//
//  ServerManager.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import SwiftKeychainWrapper

typealias ServerResponse = (_ response: JSON) -> Void

class ServerManager: NSObject {
    
    private static let _instance = ServerManager()
    
    private var manager: SessionManager!
    
    static var shared: ServerManager {
        
        return _instance
    }
    
    override init() {
        super.init()
        
        manager = getConfigForManager()
        manager.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            
            var redirectedRequest = request
            
            if let originalRequest = task.originalRequest, let headers = originalRequest.allHTTPHeaderFields, let authorizationHeaderValue = headers["Authorization"] {
                
                let mutableRequest = request as! NSMutableURLRequest
                mutableRequest.setValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
                redirectedRequest = mutableRequest as URLRequest
                
            }
            
            return redirectedRequest
        }
    }
    
    func getConfigForManager() -> SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30 //seconds
        configuration.timeoutIntervalForResource = 30 //seconds
        
        return Alamofire.SessionManager(configuration: configuration)
    }
    
    //MARK: - Пользователи
    
    // MARK: Функция регистрации пользователя
    func registerUser(phone: String, serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/user/recovery"
        
        let parameters = ["phone": phone]
        
        let headers: HTTPHeaders = [ "Accept": "application/json"]

        manager.request(Constant.SERVER_URL + URI, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    // MARK: Функция авторизации и получения внутреннего токена
    func authUser(phone: String, code: String, serverResponse: @escaping ServerResponse) -> Void {
        let URI = "/user/auth"
        
        let parameters = ["phone": phone, "password": code]
        let headers: HTTPHeaders = [ "Accept": "application/json"]

        manager.request(Constant.SERVER_URL + URI, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { [unowned self] response in
                
                self.handleGenericResponse(response: response, serverResponse: serverResponse)
        }
    }
    
    
    func handleGenericResponse(response: DataResponse<Any>, serverResponse: @escaping ServerResponse) {
        
        if response.response?.statusCode == 401 {
            self.handleUnauthorizedRequest()
            return
        }
        
        switch response.result {
        case .success:
            if let value = response.result.value {
                
                let json = JSON(value)
                
                serverResponse(json)
                
            }
        case .failure(let error):
            
            print(response.response.debugDescription)
            print(response.request?.url?.absoluteString ?? "")
            
            let statusCode = response.response?.statusCode ?? 0
            
            if statusCode >= 500 {
                self.handleServerErrors()
            } else {
                self.handleFailureInternetConnection(error: error)
            }
            
        }
        
    }
    
    func handleServerErrors() {
        
        showAlert(title: "Ошибка", message: "Произошла внутренняя ошибка сервера, попытайтесь повторить данный запрос попозже")

    }
    
    func handleUnauthorizedRequest() {
        
        UserDefaults.standard.set(false, forKey: Constant.IS_AUTHORIZED)
        UIApplication.shared.endIgnoringInteractionEvents()
        
        if let window = UIApplication.shared.keyWindow {
            
            let presenter = SignInPresenter()
            let signInVC = SignInVC(with: presenter)
            let navigation = UINavigationController(rootViewController: signInVC)
            window.rootViewController?.present(navigation, animated: true, completion: nil)
        }
        
    }
    
    func handleFailureInternetConnection(error: Error) {
        
        //NSURLErrorNotConnectedToInternet
        if error._code == -1009 {
            
            print("timeout exceeded")
            
            showAlert(title: "", message: "Время ответа от сервера превысило допустимую норму. Проверьте интернет-соединение")
            
        }
        
    }
    
    func showAlert(title: String, message: String) {
        
        if let vc = UIApplication.shared.keyWindow?.getVisibleVC() {
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
            
            vc.present(alert, animated: true, completion: nil)
            
        }

    }
    
}
