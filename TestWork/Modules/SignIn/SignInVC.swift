//
//  SignInVC.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {
    
    var presenter: SignInPresenter!

    fileprivate var mainView: SignInView {
        return self.view as! SignInView
    }
    
    override func loadView() {
        view = SignInView(controller: self)
    }
    
    
    init(with presenter: SignInPresenter) {
        super.init(nibName: nil, bundle: nil)
        self.presenter = presenter
        self.presenter.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = ""
        
        if #available(iOS 11.0, *) {
            mainView.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - Handlers
    
    func getCode(phone: String?) {
        
        guard let phone = phone else { return }
        
        if phone == "" {
            
            let alert = UIAlertController(title: "Внимание", message: "Заполните поле с телефоном", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        let validPhone = phone.replaceOccurrences(["+", "(", ")", "-", " "])

        mainView.startProgressing()
        
        presenter.registerUser(phone: validPhone)
        
    }

}

extension SignInVC: SignInProtocol {
    
    func userRegistered(success: Bool, errorString: String?) {
        
        self.mainView.stopProgressing()
        
        if success {
            
            let nextPresenter = SignInNextPresenter()
            let signInNextVC = SignInNextVC(with: nextPresenter, phone: presenter.phone)
            
            self.navigationController?.pushViewController(signInNextVC, animated: true)
            
        } else {
            
            let alert = UIAlertController(title: "Ошибка", message: errorString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }

        
    }
    
}


