//
//  SignInView.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import PinLayout
import VMaskTextField

class SignInView: BaseScrollView {
    
    weak var controller: SignInVC!
    
    fileprivate var phoneLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Введите ваш номер телефона"
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return label
        
    }()
    
    
    fileprivate lazy var phoneTextField: VMaskTextField = {
        
        let textField = VMaskTextField()
        textField.text = "+7 "
        textField.mask = "+7 (###) ###-##-##"
        textField.delegate = self
        textField.keyboardType = .numberPad
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "Номер телефона"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return textField
        
    }()
    
    fileprivate lazy var acceptButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("ПОДТВЕРДИТЬ", for: .normal)
        button.titleLabel?.font = UIFont(name: "SFCompactText-Regular", size: 16)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(acceptButtonDidTap), for: .touchUpInside)
        
        return button
        
    }()
    
    fileprivate var activityIndicator: UIActivityIndicatorView = {
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = .white
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
        
    }()
    
    init(controller: SignInVC) {
        super.init(disableTapGesture: true)
        self.controller = controller
        
        self.backgroundColor = Constant.BASE_APP_COLOR
        
        scrollView.addSubview(phoneLabel)
        scrollView.addSubview(phoneTextField)
        scrollView.addSubview(acceptButton)
        scrollView.addSubview(activityIndicator)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        phoneLabel.pin.topLeft().right().height(20).margin(44, 24, 0)
        phoneTextField.pin.below(of: phoneLabel).left().right().height(40).margin(8, 16, 0)
        
        acceptButton.pin.below(of: phoneTextField, aligned: .right).width(120).height(40).margin(16, 0, 0)
        
        activityIndicator.pin.hCenter().vCenter(to: acceptButton.edge.vCenter)
        
        // Adjust UIScrollView contentSize
        scrollView.contentSize = CGSize(width: self.bounds.width, height: acceptButton.frame.maxY + 16)
        
    }
    
    
    //MARK: - Custom Methods
    @objc func acceptButtonDidTap() {
        
        let phone = phoneTextField.text
        
        self.endEditing(true)
        
        controller.getCode(phone: phone)
        
    }
    
    func startProgressing() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopProgressing() {
        UIApplication.shared.endIgnoringInteractionEvents()
        activityIndicator.stopAnimating()
    }
    
    
}

extension SignInView: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField is VMaskTextField {
            return phoneTextField.shouldChangeCharacters(in: range, replacementString: string)
        }
        
        return true
        
    }
    
}
