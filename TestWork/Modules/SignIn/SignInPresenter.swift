//
//  SignInPresenter.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

protocol SignInProtocol: AnyObject {
    func userRegistered(success: Bool, errorString: String?)
}

class SignInPresenter: NSObject {

    weak var delegate: SignInProtocol?
    
    var phone: String!
    
    func registerUser(phone: String) {
        
        self.phone = phone
        
        ServerManager.shared.registerUser(phone: phone) { [weak self] (json) in
            
            print(json)
            
            if let errors = json.array {
                
                var array = [String]()
                
                for error in errors {
                    array.append(error["phone"].stringValue)
                }
                
                let errorString = array.joined(separator: "\n")
                
                self?.delegate?.userRegistered(success: false, errorString: errorString)
                
                return
                
            }
            
            if let dict = json["error"].dictionary {
                
                var errorString = ""
                
                for key in dict.keys {
                    errorString = dict[key]!.stringValue
                }
                
                self?.delegate?.userRegistered(success: false, errorString: errorString)

                return
            }
            
            if json["status"].string != nil {
                
                self?.delegate?.userRegistered(success: true, errorString: nil)
                
                return
            }
                        
        }

        
    }
    
}
