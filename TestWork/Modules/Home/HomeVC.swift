//
//  HomeVC.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Выход", style: .plain, target: self, action: #selector(exit))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @objc func exit() {
        
        let alert = UIAlertController(title: "Выход", message: "Вы действительно хотите выйти из профиля?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Да", style: .destructive) { [weak self] (action) in
            
            self?.showLoginVC()
        })
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showLoginVC() {
        
        if let window = UIApplication.shared.keyWindow {
            
            UserDefaults.standard.set(false, forKey: Constant.IS_AUTHORIZED)
            
            let presenter = SignInPresenter()
            let signInVC = SignInVC(with: presenter)
            let navigation = UINavigationController(rootViewController: signInVC)
            window.rootViewController?.present(navigation, animated: true, completion: nil)
            
        }
        
    }

    

}
