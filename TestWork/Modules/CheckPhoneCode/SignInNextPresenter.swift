//
//  SignInNextPresenter.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

protocol SignInNextProtocol: AnyObject {
    func userAuthenticated(success: Bool, token: String?, errorString: String?)
}

class SignInNextPresenter: NSObject {

    weak var delegate: SignInNextProtocol?

    func auth(phone: String, code: String) {
        
        ServerManager.shared.authUser(phone: phone, code: code) { [weak self] (json) in
            
            print(json)
            
            if let dict = json["error"].dictionary {
                
                var errorString = ""
                
                for key in dict.keys {
                    errorString = dict[key]!.stringValue
                }
                
                self?.delegate?.userAuthenticated(success: false, token: nil, errorString: errorString)

                return
            }
            
            if let token = json["authToken"].string {
                
                self?.delegate?.userAuthenticated(success: true, token: token, errorString: nil)

                return
            }
            
        }
        
    }
    
}
