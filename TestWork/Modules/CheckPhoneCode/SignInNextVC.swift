//
//  SignInNextVC.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class SignInNextVC: UIViewController {
    
    var presenter: SignInNextPresenter!
    var phone: String!
    
    fileprivate var mainView: SignInNextView {
        return self.view as! SignInNextView
    }
    
    override func loadView() {
        view = SignInNextView(controller: self)
    }
    
    init(with presenter: SignInNextPresenter, phone: String) {
        super.init(nibName: nil, bundle: nil)
        self.presenter = presenter
        self.phone = phone
        self.presenter.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Авторизация"
        
        if #available(iOS 11.0, *) {
            mainView.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - Handlers
    
    func auth(code: String?) {
        
        if code == nil || code == "" {
            
            let alert = UIAlertController(title: "Внимание", message: "Введите код подтверждения", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        mainView.startProgressing()
        
        presenter.auth(phone: phone, code: code!)
        
    }
    
    func passToCreateOrderVC() {
        
        let window = UIApplication.shared.keyWindow
        
        let navigation = UINavigationController(rootViewController: HomeVC())
        
        window?.rootViewController = navigation
        
    }
    
    
}

extension SignInNextVC: SignInNextProtocol {
    
    func userAuthenticated(success: Bool, token: String?, errorString: String?) {

        self.mainView.stopProgressing()
        
        if success {
            
            Constant.INNER_TOKEN = token!
            Constant.USER_PHONE = self.phone
            KeychainWrapper.standard.set(token!, forKey: "inner_token")
            
            UserDefaults.standard.set(self.phone, forKey: "phone")
            UserDefaults.standard.set(true, forKey: Constant.IS_AUTHORIZED)
            
            self.passToCreateOrderVC()
            
        } else {
            
            let alert = UIAlertController(title: "Ошибка", message: errorString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }

        
    }
    
}
