//
//  SignInNextView.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit
import PinLayout

class SignInNextView: BaseScrollView {
    
    weak var controller: SignInNextVC!
    
    fileprivate var infoLabel: UILabel = {
        
        let label = UILabel()
        label.text = "На указанный номер телефона будет отправлена СМС с проверочным кодом"
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(name: "SFCompactDisplay-Regular", size: 22)
        
        return label
        
    }()
    
    fileprivate var codeTextField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.placeholder = "Код подтверждения"
        textField.tintColor = Constant.BASE_APP_COLOR
        textField.font = UIFont(name: "SFCompactText-Regular", size: 16)
        
        return textField
        
    }()
    
    fileprivate lazy var acceptButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("ПОДТВЕРДИТЬ", for: .normal)
        button.titleLabel?.font = UIFont(name: "SFCompactText-Regular", size: 16)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(acceptButtonDidTap), for: .touchUpInside)
        
        return button
        
    }()
    
    fileprivate var activityIndicator: UIActivityIndicatorView = {
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = .white
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
        
    }()
    
    
    init(controller: SignInNextVC) {
        super.init(disableTapGesture: true)
        self.controller = controller
        
        self.backgroundColor = Constant.BASE_APP_COLOR

        scrollView.addSubview(infoLabel)
        scrollView.addSubview(codeTextField)
        scrollView.addSubview(acceptButton)
        scrollView.addSubview(activityIndicator)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutUI()
    }
    
    fileprivate func layoutUI() {
        
        infoLabel.pin.topLeft().right().margin(44, 24, 0).sizeToFit(.width)
        codeTextField.pin.below(of: infoLabel).left().right().height(40).margin(24, 16, 0)
        
        acceptButton.pin.below(of: codeTextField, aligned: .right).width(120).height(40).margin(16, 0, 0)
        
        activityIndicator.pin.hCenter().vCenter(to: acceptButton.edge.vCenter)
        
        // Adjust UIScrollView contentSize
        scrollView.contentSize = CGSize(width: self.bounds.width, height: acceptButton.frame.maxY + 16)
        
    }
    
    //MARK: - Custom Methods
    
    @objc func acceptButtonDidTap() {
        
        let code = codeTextField.text
        self.endEditing(true)
        
        controller.auth(code: code)
        
    }
    
    func startProgressing() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopProgressing() {
        UIApplication.shared.endIgnoringInteractionEvents()
        activityIndicator.stopAnimating()
    }
    
    
    
}
