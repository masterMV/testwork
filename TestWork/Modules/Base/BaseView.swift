//
//  BaseVC.swift
//  TestWork
//
//  Created by maxim vingalov on 17.08.2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

import UIKit

class BaseView: UIView {
    
    fileprivate (set) var topLayoutGuide: CGFloat = 0
    fileprivate (set) var bottomLayoutGuide: CGFloat = 0
    
    fileprivate weak var controller: UIViewController?
    
    init(controller: UIViewController) {
        super.init(frame: UIScreen.main.bounds)
        self.controller = controller
        backgroundColor = .white
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLayoutGuides(top: CGFloat, bottom: CGFloat) {
        var didChange = false
        
        if top != topLayoutGuide {
            topLayoutGuide = top
            didChange = true
        }
        
        if bottom != bottomLayoutGuide {
            bottomLayoutGuide = bottom
            didChange = true
        }
        
        if didChange {
            didChangeLayoutGuides()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        topLayoutGuide = controller?.topLayoutGuide.length ?? 0
        bottomLayoutGuide = controller?.bottomLayoutGuide.length ?? 0
        
    }
    
    //MARK: - this methods must be overrided for successors
    
    func keyboardWillShow(keyboardHeight: CGFloat) {
        
    }
    
    func keyboardWillHide() {
        
    }
    
    
    @objc func keyboardWillShow(notification: Notification) {
        guard let sizeValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
        keyboardWillShow(keyboardHeight: sizeValue.cgRectValue.height)
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        keyboardWillHide()
    }
    
    
    func didChangeLayoutGuides() {
        setNeedsLayout()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
}
